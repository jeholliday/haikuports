SUMMARY="A tool for downloading files from the internet"
DESCRIPTION="
wget is a tool that can download files from the internet through protocols \
such as HTTP, HTTPS and FTP. wget is non-interactive, so it can be called from \
scripts.
"
HOMEPAGE="http://www.gnu.org/software/wget" 
SRC_URI="http://ftp.gnu.org/gnu/wget/wget-$portVersion.tar.gz"
CHECKSUM_SHA256="b977fc10ac7a72d987d48136251aeb332f2dced1aabd50d6d56bdf72e2b79101"
LICENSE="GNU GPL v3"
COPYRIGHT="1996-2014 Free Software Foundation"
REVISION="1"
ARCHITECTURES="x86_gcc2 x86 x86_64"
SECONDARY_ARCHITECTURES="x86 x86_gcc2"

PROVIDES="
	wget$secondaryArchSuffix = $portVersion
	cmd:wget$secondaryArchSuffix = $portVersion
	"

REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	lib:libcrypto$secondaryArchSuffix
	lib:libssl$secondaryArchSuffix
	lib:libz$secondaryArchSuffix
	"

BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	devel:libcrypto$secondaryArchSuffix
	devel:libssl$secondaryArchSuffix
	devel:libz$secondaryArchSuffix >= 1.2.3
	"
BUILD_PREREQUIRES="
	cmd:gcc$secondaryArchSuffix
	cmd:aclocal
	cmd:autoconf
	cmd:libtoolize
	cmd:make
	cmd:makeinfo
	cmd:pod2man
	"

USER_SETTINGS_FILES="
	settings/wgetrc
	"

GLOBAL_WRITABLE_FILES="
	settings/wgetrc keep-old
"

PATCHES="wget-1.16.patchset"

BUILD()
{
	libtoolize --force --copy --install
	aclocal -I m4
	autoconf
	runConfigure ./configure --with-ssl=openssl
	make
}

INSTALL()
{
	make install
	# Tells wget where to find certificates
	echo "ca_certificate=/boot/system/data/ssl/CARootCertificates.pem" > $settingsDir/wgetrc
}

TEST()
{
	[ -d .git ] && mv .git .git-temp
	make check || true
	[ -d .git-temp ] && mv .git-temp .git
}
