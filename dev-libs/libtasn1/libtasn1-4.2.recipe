SUMMARY="A small ASN.1 library"
DESCRIPTION="
The Libtasn1 library provides Abstract Syntax Notation One \
(ASN.1, as specified by the X.680 ITU-T recommendation) parsing and \
structures management, and Distinguished Encoding Rules (DER, as per \
X.690) encoding and decoding functions.
"
HOMEPAGE="http://www.gnu.org/software/libtasn1"
COPYRIGHT="
	2002-2014 Free Software Foundation, Inc.
"
LICENSE="GNU LGPL v2.1
GNU GPL v3"
SRC_URI="http://ftp.gnu.org/gnu/libtasn1/libtasn1-$portVersion.tar.gz"
CHECKSUM_SHA256="693b41cb36c2ac02d5990180b0712a79a591168e93d85f7fcbb75a0a0be4cdbb"
REVISION="1"
ARCHITECTURES="x86_gcc2 x86 x86_64"
SECONDARY_ARCHITECTURES="x86_gcc2 x86"

PROVIDES="
	libtasn1$secondaryArchSuffix = $portVersion compat >= 4
	lib:libtasn1$secondaryArchSuffix = 6.3.2 compat >= 6
	cmd:asn1Coding$secondaryArchSuffix
	cmd:asn1Decoding$secondaryArchSuffix
	cmd:asn1Parser$secondaryArchSuffix
	"
REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	"
BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	"
BUILD_PREREQUIRES="
	cmd:aclocal
	cmd:autoconf
	cmd:automake
	cmd:gcc$secondaryArchSuffix
	cmd:ld$secondaryArchSuffix
	cmd:libtoolize
	cmd:make
	cmd:pkg_config$secondaryArchSuffix
	"

BUILD()
{
	autoreconf -fi
	runConfigure ./configure
	make $jobArgs
}

INSTALL()
{
	make install

	prepareInstalledDevelLibs libtasn1
	fixPkgconfig

	# devel package
	packageEntries devel \
		$developDir \
		$manDir/man3
}

# ----- devel package -------------------------------------------------------

PROVIDES_devel="
	libtasn1${secondaryArchSuffix}_devel = $portVersion compat >= 4
	devel:libtasn1$secondaryArchSuffix = 6.3.2 compat >= 6
	"
REQUIRES_devel="
	libtasn1$secondaryArchSuffix == $portVersion base
	"
