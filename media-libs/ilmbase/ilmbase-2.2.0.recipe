DESCRIPTION="
IlmBase provides base libraries for OpenEXR. It contains five \
modules. Half encapsulates 16-bit floating-point format. IlmThread \
is thread abstraction library which currently supports pthreads and \
Windows threads. Imath implements 2D and 3D vectors, 3x3 and 4x4 \
matrices, quaternions and other useful math functions. Iex is an \
exception-handling library. There is also IexMath module.
"
SUMMARY="OpenEXR ILM Base libraries"
HOMEPAGE="http://www.openexr.com"
LICENSE="BSD (3-clause)"
COPYRIGHT="2002-2011 Industrial Light & Magic"
SRC_URI="http://download.savannah.nongnu.org/releases/openexr/ilmbase-$portVersion.tar.gz"
CHECKSUM_SHA256="ecf815b60695555c1fbc73679e84c7c9902f4e8faa6e8000d2f905b8b86cedc7"
REVISION="1"

ARCHITECTURES="x86 x86_64"
if [ $effectiveTargetArchitecture != x86_gcc2 ]; then
	# x86_gcc2 is fine as primary target architecture as long as we're building
	# for a different secondary architecture.
	ARCHITECTURES="$ARCHITECTURES x86_gcc2"
else
	ARCHITECTURES="$ARCHITECTURES !x86_gcc2"
fi
SECONDARY_ARCHITECTURES="x86"

PROVIDES="
	ilmbase$secondaryArchSuffix = $portVersion
	lib:libHalf$secondaryArchSuffix = 12.0.0 compat >= 12
	lib:libIex_2_2$secondaryArchSuffix = 12.0.0 compat >= 12
	lib:libIexMath_2_2$secondaryArchSuffix = 12.0.0 compat >= 12
	lib:libIlmThread_2_2$secondaryArchSuffix = 12.0.0 compat >= 12
	lib:libImath_2_2$secondaryArchSuffix = 12.0.0 compat >= 12
	"

REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	lib:libstdc++$secondaryArchSuffix
	"
BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	"
BUILD_PREREQUIRES="
	cmd:gcc$secondaryArchSuffix
	cmd:ld$secondaryArchSuffix
	cmd:make
	cmd:which
	cmd:autoconf
	cmd:aclocal
	cmd:libtoolize
	"
	
BUILD()
{
	./bootstrap
	runConfigure ./configure
	make
}

INSTALL()
{
	make install
	
	# prepare development lib links
	prepareInstalledDevelLibs libHalf libIex libIexMath libIlmThread \
		libImath libIex-2_2 libIexMath-2_2 libIlmThread-2_2 libImath-2_2
	
	fixPkgconfig
	
	# devel package
	packageEntries devel $developDir
}

TEST()
{
	make check
}

# ----- devel package -------------------------------------------------------

PROVIDES_devel="
	ilmbase${secondaryArchSuffix}_devel = $portVersion
	devel:libHalf$secondaryArchSuffix = 12.0.0 compat >= 12
	devel:libIex_2_2$secondaryArchSuffix = 12.0.0 compat >= 12
	devel:libIexMath_2_2$secondaryArchSuffix = 12.0.0 compat >= 12
	devel:libIlmThread_2_2$secondaryArchSuffix = 12.0.0 compat >= 12
	devel:libImath_2_2$secondaryArchSuffix = 12.0.0 compat >= 12
	devel:libIex$secondaryArchSuffix = 12.0.0 compat >= 12
	devel:libIexMath$secondaryArchSuffix = 12.0.0 compat >= 12
	devel:libIlmThread$secondaryArchSuffix = 12.0.0 compat >= 12
	devel:libImath$secondaryArchSuffix = 12.0.0 compat >= 12
	"
REQUIRES_devel="
	ilmbase$secondaryArchSuffix == $portVersion base
	"
