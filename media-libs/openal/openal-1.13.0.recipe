SUMMARY="A software implementation of the OpenAL 3D audio API"
DESCRIPTION="
OpenAL - A software implementation of the OpenAL 3D audio API.
"
HOMEPAGE="http://kcat.strangesoft.net/openal.html" 
SRC_URI="http://kcat.strangesoft.net/openal-releases/openal-soft-1.13.tar.bz2"
CHECKSUM_SHA256="dc735b8b2ab21f6ec54b4262f150a7e0527ae42aa975a1965b9342df1520443c"
REVISION="2"
LICENSE="GNU LGPL v2.1"
COPYRIGHT="1999-2000 Loki Software
	2005-2011 OpenAL Soft team"

ARCHITECTURES="x86_gcc2 x86 x86_64"
SECONDARY_ARCHITECTURES="x86_gcc2 x86"

PROVIDES="
	openal$secondaryArchSuffix = $portVersion compat >= 1
	lib:libopenal$secondaryArchSuffix = 1.13.0 compat >= 1
	"

REQUIRES="
	haiku$secondaryArchSuffix >= $haikuVersion
	"

BUILD_REQUIRES="
	haiku${secondaryArchSuffix}_devel >= $haikuVersion
	"

BUILD_PREREQUIRES="
	cmd:cmake
	cmd:gcc$secondaryArchSuffix
	cmd:ld$secondaryArchSuffix
	cmd:make
	"

SOURCE_DIR="openal-soft-1.13"

PATCHES="openal-1.13.0.patchset"
BUILD()
{
	cd build
	cmake .. -DCMAKE_INSTALL_PREFIX=$prefix -DBIN_DIR="$relativeBinDir" \
		-DLIB_SUFFIX="/${secondaryArchSuffix/_/}" 
	make $jobArgs
}

INSTALL()
{
	cd build
	make install
	
	mkdir -p $developDir/headers
	mv $prefix/include/* $developDir/headers
	rmdir $prefix/include

	prepareInstalledDevelLib libopenal
	fixPkgconfig
	
	# devel package
	packageEntries devel \
			$developDir \
			$binDir
}

PROVIDES_devel="
	openal${secondaryArchSuffix}_devel = $portVersion compat >= 1
	devel:libopenal$secondaryArchSuffix = 1.13.0 compat >= 0
	cmd:openal_info$secondaryArchSuffix
	"

REQUIRES_devel="
	haiku$secondaryArchSuffix >= $haikuVersion
	lib:libopenal$secondaryArchSuffix == $portVersion base
	"
